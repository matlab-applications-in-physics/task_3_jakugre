function weathers()
    %open file in appending mode
    result_file = fopen('weather_data.csv','a+');
    %3 nearest stations
    stations = ["raciborz","bielskobiala","katowice"];
    %URL fragments for each page
    imgw_url = 'https://danepubliczne.imgw.pl/api/data/synop/station/';
    wttr_url = 'https://wttr.in/';
    wttr_json = '?format=j1';
    disp('x');
    
    %Clearing file from empty rows
    table = readlines('weather_data.csv');
    for row = 1:length(table(:))
        if strlength(table(row)) == 0
            table(row)=[];
        end
    end
    
  
    
    
    %Printing headlines
    fprintf(result_file, 'station; data pomiaru; time; temp; pressure; speed; humidity; sunrise; sunset; perceived temperature \n');
    
    for i = [1:length(stations)]
        format = '%s; %s; %s; %s; %s; %s; %s; %s; %s; %.1f\n';
        %Creating URL for each station and page
        station_wttr_url = strcat(wttr_url,stations(i),wttr_json);
        station_imgw_url = strcat(imgw_url,stations(i));
        
        %Downloading and processing data required for task
        date_station_imgw = webread(station_imgw_url).data_pomiaru;
        temp_station_imgw = webread(station_imgw_url).temperatura;
        press_station_imgw = webread(station_imgw_url).cisnienie;
        speed_station_imgw = webread(station_imgw_url).predkosc_wiatru;
        hum_station_imgw = webread(station_imgw_url).wilgotnosc_wzgledna;
        time_station_imgw = webread(station_imgw_url).godzina_pomiaru;
        
        %Calculating perceived temperature by formula suggested on
        %Wikipedia
        perc_temp_imgw = 13.12 + 0.6215*str2double(temp_station_imgw) - 11.37*(str2double(speed_station_imgw)^0.16) + 0.3965*str2double(temp_station_imgw)*(str2double(speed_station_imgw)^0.16);
        
        
        date_station_wttr = webread(station_wttr_url).current_condition.localObsDateTime;
        temp_station_wttr = webread(station_wttr_url).current_condition.temp_C;
        press_station_wttr = webread(station_wttr_url).current_condition.pressure;
        speed_station_wttr = webread(station_wttr_url).current_condition.windspeedKmph;
        hum_station_wttr = webread(station_wttr_url).current_condition.humidity;
        time_station_wttr = webread(station_wttr_url).current_condition.observation_time;
        
        %Reading sunrise and sunset times 
        sunrise = webread(station_wttr_url).weather(1).astronomy.sunrise;
        sunset = webread(station_wttr_url).weather(1).astronomy.sunset;
        
        perc_temp_wttr = 13.12 + 0.6215*str2double(temp_station_wttr) - 11.37*(str2double(speed_station_wttr)^0.16) + 0.3965*str2double(temp_station_wttr)*(str2double(speed_station_wttr)^0.16);
        
       
        
        %Printing data to the file
        fprintf(result_file,format,stations(i)+'imgw',date_station_imgw,num2str(time_station_imgw)+":00",temp_station_imgw,press_station_imgw,speed_station_imgw,hum_station_imgw,sunrise,sunset,perc_temp_imgw);
        fprintf(result_file,format,stations(i)+'wttr',date_station_wttr(1:10),time_station_wttr,temp_station_wttr,press_station_wttr,speed_station_wttr,hum_station_wttr,sunrise,sunset,perc_temp_wttr);
        
        disp([result_file,format,stations(i)+'imgw',date_station_imgw,num2str(time_station_imgw)+":00",temp_station_imgw,press_station_imgw,speed_station_imgw,hum_station_imgw,sunrise,sunset,perc_temp_imgw])
        disp([result_file,format,stations(i)+'wttr',date_station_wttr(1:10),time_station_wttr,temp_station_wttr,press_station_wttr,speed_station_wttr,hum_station_wttr,sunrise,sunset,perc_temp_wttr])
    end
    %Closing file and executing function that will check if there are any
    %repetitions
    fclose(result_file);
    weather_check_data();
    
end