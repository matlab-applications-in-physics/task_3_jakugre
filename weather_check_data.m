function check_if_repeats()
    %Reading file into the table and opening it into writing mode so it can
    %be rewritten
    results = readlines('weather_data.csv');
    file = fopen('weather_data.csv','w');
    
    %Clearing file of empty rows so they don't make mess 
    for k = 1:length(results)
        if strlength(results(k)) == 0
           results(k)=[];
        end
    end
    
    %Deleteging spaces from all lines so that we can find repeating ones
    results = replace(results," ","");
    
    %Unique function deletes any objects that repeat within given argument
    result = unique(results,'stable');
    
   
    %Rewriting lines without repetitions to the file
    for i = [1:length(result)]
        fprintf(file,"%s \n", result(i));
        result(i);
    end
    
    fclose(file);
end


