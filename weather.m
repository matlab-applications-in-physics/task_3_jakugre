%MATLAB 2020b
%name: weather
%author: jakugre
%date: 2020-11-23
%version: v1.0

%Create timer object that will run every 20 minutes in case there is delay
%on any page with posting data - repeated sets will be deleted later
t = timer;
t.ExecutionMode = 'fixedSpacing';
t.Period = 1200;
t.TimerFcn = @(~,~)weather_function;




start(t)


